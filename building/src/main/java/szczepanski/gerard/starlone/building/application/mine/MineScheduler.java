package szczepanski.gerard.starlone.building.application.mine;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.starlone.building.application.storehouse.Storehouse;
import szczepanski.gerard.starlone.building.application.storehouse.StorehouseRepository;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
class MineScheduler {

    private final MineRepository mineRepository;
    private final StorehouseRepository storehouseRepository;
    private final MotherLodeClient motherLodeClient;

    /**
     * FOR FUTURE OPTIMIZATION!!!
     *
     * VERY NAIVE IMPL ONLY FOR PROCESS CHEKING
     */
    @Transactional
    @Scheduled(cron = "0 0 0/2 * * ?")
    public void scheduledMine() {
        log.debug("START MINING");
        Map<DomainId, Set<Storehouse>> storehousesPerPlanetMap = new HashMap<>();

        Set<Mine> mines = mineRepository.findAll();

        for (Mine mine: mines) {
            DomainId planetId = mine.getPlanetId();

            if (!storehousesPerPlanetMap.containsKey(planetId)) {
                storehousesPerPlanetMap.put(planetId, storehouseRepository.findByPlanetId(planetId));
            }

            Set<Storehouse> storehouses = storehousesPerPlanetMap.get(planetId);
            mine.mine(storehouses, motherLodeClient);
        }

        saveStorehouses(storehousesPerPlanetMap);
        log.debug("FINISH MINING");
    }

    private void saveStorehouses(Map<DomainId, Set<Storehouse>> storehousesPerPlanetMap) {
        storehousesPerPlanetMap
                    .values()
                    .forEach(storehouseRepository::save);
    }


}
