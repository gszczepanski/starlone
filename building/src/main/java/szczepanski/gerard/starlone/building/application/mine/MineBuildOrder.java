package szczepanski.gerard.starlone.building.application.mine;

import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(schema = "BUILDING", name = "MINE_BUILD_ORDER")
public class MineBuildOrder {

    @Id
    private String id;

    @Column(name = "PLANET_ID")
    @Convert(converter = DomainIdConverter.class)
    private DomainId planetId;

    @Column(name = "MOTHER_LODE_ID")
    @Convert(converter = DomainIdConverter.class)
    private DomainId motherLodeId;

    @Column(name = "MINING_UNITS_PER_DAY")
    private Integer miningUnitsPerDay;

    @Column(name = "BUILD_DATE")
    private LocalDateTime buildDate;

    @Column(name = "BUILD_DAYS")
    private Integer buildDays;

    @Column(name = "IS_FINISHED")
    private Boolean finished;

    MineBuildOrder orderMine(DomainId motherLodeId, MotherLodeClient motherLodeClient) {

        return new MineBuildOrder();
    }



}
