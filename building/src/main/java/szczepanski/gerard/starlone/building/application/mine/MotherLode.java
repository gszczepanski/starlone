package szczepanski.gerard.starlone.building.application.mine;

import lombok.AllArgsConstructor;
import lombok.Value;
import szczepanski.gerard.starlone.building.application.storehouse.ResourceType;
import szczepanski.gerard.starlone.commons.id.DomainId;

@Value
@AllArgsConstructor
public class MotherLode {

    private DomainId id;
    private ResourceType resourceType;

    MotherLode() {
        this.id = null;
        this.resourceType = null;
    }
}
