package szczepanski.gerard.starlone.building.infrastructure.jpa;

import lombok.RequiredArgsConstructor;
import org.apache.catalina.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import szczepanski.gerard.starlone.building.application.storehouse.Storehouse;
import szczepanski.gerard.starlone.building.application.storehouse.StorehouseRepository;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Set;

@Repository
@RequiredArgsConstructor
class StorehouseRepositoryAdapter implements StorehouseRepository {

    private final StorehouseJpaRepository jpaRepository;

    @Override
    public void save(Storehouse storehouse) {
        jpaRepository.save(storehouse);
    }

    @Override
    public void save(Set<Storehouse> storehouses) {
        jpaRepository.save(storehouses);
    }

    @Override
    public Set<Storehouse> findByPlanetId(DomainId planetId) {
        return jpaRepository.findByPlanetId(planetId);
    }

    @Repository
    interface StorehouseJpaRepository extends JpaRepository<Storehouse, String> {
        @Query("SELECT s FROM Storehouse s WHERE s.planetId = :planetId")
        Set<Storehouse> findByPlanetId(@Param("planetId") DomainId planetId);
    }
}
