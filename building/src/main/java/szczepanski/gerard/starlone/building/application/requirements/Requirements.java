package szczepanski.gerard.starlone.building.application.requirements;

import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import szczepanski.gerard.starlone.building.application.storehouse.ResourceUnits;

import java.util.Set;

import static lombok.AccessLevel.PRIVATE;
import static szczepanski.gerard.starlone.building.application.storehouse.ResourceType.CRYPTOBLOCKS;
import static szczepanski.gerard.starlone.building.application.storehouse.ResourceType.METAL;

@AllArgsConstructor(access = PRIVATE)
public class Requirements {

    private final Set<ResourceUnits> requiredResources;

    public static Requirements smallStorehouse() {
        return new Requirements(
                Sets.newHashSet(
                        new ResourceUnits(METAL, 200),
                        new ResourceUnits(CRYPTOBLOCKS, 300)
                )
        );
    }

    public static Requirements mediumStorehouse() {
        return new Requirements(
                Sets.newHashSet(
                        new ResourceUnits(METAL, 400),
                        new ResourceUnits(CRYPTOBLOCKS, 600)
                )
        );
    }

    public static Requirements largeStorehouse() {
        return new Requirements(
                Sets.newHashSet(
                        new ResourceUnits(METAL, 900),
                        new ResourceUnits(CRYPTOBLOCKS, 1000)
                )
        );
    }

}
