package szczepanski.gerard.starlone.building.application.storehouse;

public enum ResourceType {

    CRYSTAL,
    METAL,
    NAPHTHA,
    ENERGY,
    CRYPTOBLOCKS;

}
