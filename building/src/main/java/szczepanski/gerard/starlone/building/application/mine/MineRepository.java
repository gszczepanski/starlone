package szczepanski.gerard.starlone.building.application.mine;

import java.util.Set;

public interface MineRepository {

    void save(Mine mine);

    Set<Mine> findAll();

}
