package szczepanski.gerard.starlone.building.application.building;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.starlone.building.application.mine.Mine;
import szczepanski.gerard.starlone.building.application.mine.MotherLode;
import szczepanski.gerard.starlone.building.application.mine.MotherLodeClient;
import szczepanski.gerard.starlone.building.application.storehouse.ResourceUnits;
import szczepanski.gerard.starlone.building.application.storehouse.Storehouse;
import szczepanski.gerard.starlone.building.application.storehouse.Storehouse.StorehouseSize;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.google.common.base.Preconditions.checkNotNull;
import static szczepanski.gerard.starlone.building.application.storehouse.ResourceType.*;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class NewUserBuildingSet {

    private static final Random RANDOM = new Random();

    private final Storehouse storehouse;
    private final Mine mine;

    public static NewUserBuildingSet generateForPlanet(DomainId planetId, MotherLodeClient motherLodeClient) {
        checkNotNull(planetId, "Planet Id must not be null!");

        return new NewUserBuildingSet(
                defaultStorehouse(planetId),
                defaultMine(planetId, motherLodeClient)
        );
    }

    private static Storehouse defaultStorehouse(DomainId planetId) {
        Storehouse storehouse = new Storehouse(planetId, StorehouseSize.SMALL);

        storehouse.addResource(new ResourceUnits(CRYPTOBLOCKS, 300));
        storehouse.addResource(new ResourceUnits(CRYSTAL, 100));
        storehouse.addResource(new ResourceUnits(METAL, 500));
        storehouse.addResource(new ResourceUnits(ENERGY, 400));
        storehouse.addResource(new ResourceUnits(NAPHTA, 300));

        return storehouse;
    }

    private static Mine defaultMine(DomainId planetId, MotherLodeClient motherLodeClient) {
        MotherLode motherLode = randomMotherlode(planetId, motherLodeClient);
        return new Mine(planetId, motherLode.getId(), 10);
    }

    private static MotherLode randomMotherlode(DomainId planetId, MotherLodeClient motherLodeClient) {
        List<MotherLode> allForPlanet = new ArrayList(motherLodeClient.findAllForPlanet(planetId));
        int size = allForPlanet.size();
        return allForPlanet.get(RANDOM.nextInt(size));
    }

}
