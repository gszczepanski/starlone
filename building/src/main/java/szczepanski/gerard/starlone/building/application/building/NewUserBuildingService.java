package szczepanski.gerard.starlone.building.application.building;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import szczepanski.gerard.starlone.building.application.mine.MineRepository;
import szczepanski.gerard.starlone.building.application.mine.MotherLodeClient;
import szczepanski.gerard.starlone.building.application.storehouse.StorehouseRepository;
import szczepanski.gerard.starlone.commons.id.DomainId;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class NewUserBuildingService {

    private final StorehouseRepository storehouseRepository;
    private final MineRepository mineRepository;
    private final MotherLodeClient motherLodeClient;

    @Transactional
    public void createNewUserBuildings(DomainId planetId) {
        NewUserBuildingSet buildingSet = NewUserBuildingSet.generateForPlanet(planetId, motherLodeClient);

        storehouseRepository.save(buildingSet.getStorehouse());
        mineRepository.save(buildingSet.getMine());
    }

}
