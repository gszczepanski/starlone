package szczepanski.gerard.starlone.building.application.mine;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.starlone.commons.id.DomainId;

@Slf4j
@Service
@RequiredArgsConstructor
public class MineService {

    private final MineBuildValidator buildValidator;
    private final MotherLodeClient motherLodeClient;

    @Transactional
    public void buildMine(DomainId motherLodeId) {
        buildValidator.validateBuildPossibility(motherLodeId);

    }

}
