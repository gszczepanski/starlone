package szczepanski.gerard.starlone.building.boundary.building;

import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.building.application.building.NewUserBuildingService;
import szczepanski.gerard.starlone.commons.events.Events;
import szczepanski.gerard.starlone.commons.events.PlanetAssignedToNewUserEvent;
import szczepanski.gerard.starlone.commons.events.UserCreatedEvent;

@Slf4j
@Component
class BuildingEventListener {

    private final NewUserBuildingService newUserBuildingService;

    BuildingEventListener(Events events, NewUserBuildingService newUserBuildingService) {
        this.newUserBuildingService = newUserBuildingService;
        events.register(this);
    }

    @Subscribe
    void createBasicBuildingsOnPlanetAssignedToNewUser(PlanetAssignedToNewUserEvent event) {
        log.debug("Start creating basic buildings for planet with id: {}", event.getPlanetId());
        newUserBuildingService.createNewUserBuildings(event.getPlanetId());
    }

}
