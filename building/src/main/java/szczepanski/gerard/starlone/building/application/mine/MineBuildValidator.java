package szczepanski.gerard.starlone.building.application.mine;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.starlone.commons.id.DomainId;

@Component
@RequiredArgsConstructor
class MineBuildValidator {

    @Transactional(readOnly = true)
    void validateBuildPossibility(DomainId motherLodeId) {
        // CHECK RESOURCES
        // CHECK MOTHERLODE IS FREE (MINE NOT BUILD OR BEING ORDERED)
    }

}
