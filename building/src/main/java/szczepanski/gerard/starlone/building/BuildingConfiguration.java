package szczepanski.gerard.starlone.building;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(value = "szczepanski.gerard.starlone.building")
@EnableJpaRepositories(basePackages = "szczepanski.gerard.starlone.building.infrastructure.jpa", considerNestedRepositories = true)
@EntityScan(basePackages = "szczepanski.gerard.starlone.building.application")
public class BuildingConfiguration {
}
