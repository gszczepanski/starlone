package szczepanski.gerard.starlone.building.application.mine;

public interface MineBuildOrderRepository {

    void save(MineBuildOrder mineBuildOrder);

}
