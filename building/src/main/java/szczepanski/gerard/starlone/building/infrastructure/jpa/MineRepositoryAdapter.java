package szczepanski.gerard.starlone.building.infrastructure.jpa;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import szczepanski.gerard.starlone.building.application.mine.Mine;
import szczepanski.gerard.starlone.building.application.mine.MineRepository;

import java.util.Set;

@Repository
@RequiredArgsConstructor
class MineRepositoryAdapter implements MineRepository {

    private final MineJpaRepository mineJpaRepository;

    @Override
    public void save(Mine mine) {
        mineJpaRepository.save(mine);
    }

    @Override
    public Set<Mine> findAll() {
        return mineJpaRepository.findAllMines();
    }

    @Repository
    interface MineJpaRepository extends JpaRepository<Mine, String> {

        @Query("SELECT m FROM Mine m")
        Set<Mine> findAllMines();

    }

}
