package szczepanski.gerard.starlone.building.application.mine;

import szczepanski.gerard.starlone.building.application.storehouse.ResourceUnits;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Set;

public interface MotherLodeClient {

    Set<MotherLode> findAllForPlanet(DomainId planetId);

    ResourceUnits mineResource(DomainId motherLodeId, Integer unitsMined);

}
