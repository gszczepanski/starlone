package szczepanski.gerard.starlone.building.application.storehouse;

import org.apache.catalina.Store;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Set;

public interface StorehouseRepository {

    void save(Storehouse storehouse);

    void save(Set<Storehouse> storehouses);

    Set<Storehouse> findByPlanetId(DomainId planetId);

}
