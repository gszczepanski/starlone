package szczepanski.gerard.starlone.building.application.storehouse;

import com.google.common.collect.Sets;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;
import static szczepanski.gerard.starlone.building.application.storehouse.ResourceType.*;

@Entity
@Table(schema = "BUILDING", name = "STOREHOUSE")
@NoArgsConstructor
public class Storehouse {

    @Id
    private String id;

    @Getter
    @Column(name = "CREATION_TIME")
    private LocalDateTime creationTime;

    @Column(name = "PLANET_ID", nullable = false)
    @Convert(converter = DomainIdConverter.class)
    private DomainId planetId;

    @OneToMany(mappedBy = "storehouse", cascade = CascadeType.ALL)
    private Set<StoredResource> resources;

    @Enumerated(STRING)
    @Column(name = "SIZE", nullable = false)
    private StorehouseSize size;

    public Storehouse(DomainId planetId, StorehouseSize size) {
        checkNotNull(planetId, size);

        this.id = DomainId.create().toString();
        this.creationTime = LocalDateTime.now();
        this.planetId = planetId;
        this.resources = initStoredResources();
        this.size = size;
    }

    private Set<StoredResource> initStoredResources() {
        return Sets.newHashSet(
                new StoredResource(CRYPTOBLOCKS, this),
                new StoredResource(CRYSTAL, this),
                new StoredResource(ENERGY, this),
                new StoredResource(METAL, this),
                new StoredResource(NAPHTA, this)
        );
    }

    public void addResource(ResourceUnits units) {
        checkNotNull(units);
        StoredResource storedResource = getResourceByType(units.getType());
        storedResource.units.add(units);
    }

    private StoredResource getResourceByType(ResourceType type) {
        return resources
                .stream()
                .filter(r -> r.units.getType().equals(type))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(format("Unable to find resource with type %s in storehouse %s", type, id)));
    }

    public boolean hasEnoughCapacityFor(ResourceUnits units) {
        checkNotNull(units);
        StoredResource resourceByType = getResourceByType(units.getType());
        return resourceByType.units.getQuantity() + units.getQuantity() < size.maximumUnitsPerResource;
    }

    @Getter
    @RequiredArgsConstructor
    public enum StorehouseSize {

        SMALL(1500),
        MEDIUM(4000),
        LARGE(10000);

        private final Integer maximumUnitsPerResource;
    }

    /**
     * Represents stored resource with quantity.
     */
    @Entity
    @Table(schema = "BUILDING", name = "STORED_RESOURCE")
    @EqualsAndHashCode
    @NoArgsConstructor
    static class StoredResource {

        @Id
        private String id;

        @ManyToOne(fetch = LAZY)
        @JoinColumn(name = "STOREHOUSE_ID")
        private Storehouse storehouse;

        @Embedded
        private ResourceUnits units;

        StoredResource(ResourceType resourceType, Storehouse parent) {
            this.id = DomainId.create().toString();
            this.storehouse = parent;
            this.units = new ResourceUnits(resourceType, 0);
        }

    }
}
