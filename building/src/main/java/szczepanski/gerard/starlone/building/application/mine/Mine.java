package szczepanski.gerard.starlone.building.application.mine;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.catalina.Store;
import szczepanski.gerard.starlone.building.application.storehouse.ResourceType;
import szczepanski.gerard.starlone.building.application.storehouse.ResourceUnits;
import szczepanski.gerard.starlone.building.application.storehouse.Storehouse;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

@Entity
@Table(schema = "BUILDING", name = "MINE")
@NoArgsConstructor
@EqualsAndHashCode
public class Mine {

    @Id
    private String id;

    @Getter
    @Column(name = "CREATION_TIME")
    private LocalDateTime creationTime;

    @Getter
    @Column(name = "PLANET_ID", nullable = false)
    @Convert(converter = DomainIdConverter.class)
    private DomainId planetId;

    @Column(name = "MOTHER_LODE_ID", nullable = false)
    @Convert(converter = DomainIdConverter.class)
    private DomainId motherLodeId;

    @Column(name = "MINING_UNITS_PER_DAY", nullable = false)
    private Integer miningUnitsPerDay;

    public Mine(DomainId planetId, DomainId motherLodeId, Integer miningPerDay) {
        checkNotNull(planetId);
        checkNotNull(motherLodeId);
        checkNotNull(miningPerDay);

        this.id = DomainId.create().toString();
        this.creationTime = LocalDateTime.now();
        this.planetId = planetId;
        this.motherLodeId = motherLodeId;
        this.miningUnitsPerDay = miningPerDay;
    }

    /**
     * TODO gszczepanski
     * This process should be done differently.
     * Right now each mine throws request to server which is very unefficient.
     * To future change.
     *
     */
    void mine(Set<Storehouse> storehouses, MotherLodeClient motherLodeClient) {
        ResourceUnits resourceUnits = motherLodeClient.mineResource(motherLodeId, miningUnitsPerDay);

        for (Storehouse storehouse: storehouses) {
            if (storehouse.hasEnoughCapacityFor(resourceUnits)) {
                storehouse.addResource(resourceUnits);
                break;
            }
        }
    }

}
