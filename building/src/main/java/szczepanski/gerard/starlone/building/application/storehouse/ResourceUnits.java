package szczepanski.gerard.starlone.building.application.storehouse;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static javax.persistence.EnumType.STRING;

@Value
@Embeddable
@AllArgsConstructor
public class ResourceUnits {

    @Enumerated(STRING)
    @Column(name = "RESOURCE_TYPE", nullable = false)
    private ResourceType type;

    @Column(name = "QUANTITY", nullable = false)
    private Integer quantity;

    public ResourceUnits add(ResourceUnits units) {
        checkNotNull(units);
        checkState(this.type.equals(units.getType()), "Incompatible types");

        return new ResourceUnits(type, quantity + units.getQuantity());
    }

    // For Hibernate
    ResourceUnits() {
        type = null;
        quantity = null;
    }

}
