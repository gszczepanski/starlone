package szczepanski.gerard.starlone.building.infrastructure.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.building.application.mine.MotherLode;
import szczepanski.gerard.starlone.building.application.mine.MotherLodeClient;
import szczepanski.gerard.starlone.building.application.storehouse.ResourceUnits;
import szczepanski.gerard.starlone.commons.applicationcall.StarloneRequestProvider;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.io.IOException;
import java.util.Set;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
class MotherLodeClientAdapter implements MotherLodeClient {

    private final StarloneRequestProvider starloneRequestProvider;
    private final ObjectMapper objectMapper;

    @Override
    public Set<MotherLode> findAllForPlanet(DomainId planetId) {
        log.debug("Fetching MotherLodes for Planet with id {}", planetId);

        HttpUriRequest request = starloneRequestProvider
                .getRequest(format("/v0/api/inner/planet/%s/motherlode", planetId))
                .build();

        String json = starloneRequestProvider.executeAndGetJson(request);
        try {
            return objectMapper.readValue(json, new TypeReference<Set<MotherLode>>(){});
        } catch (IOException e) {
           log.error("Unable to convert json {} to Set of MotherLodes", json, e);
           throw new RuntimeException(e);
        }
    }

    @Override
    public ResourceUnits mineResource(DomainId motherLodeId, Integer unitsMined) {
        log.debug("Sending mine request for MotherLode {} with units mined {}", motherLodeId, unitsMined);
        HttpUriRequest request = starloneRequestProvider
                .putRequest(format("/v0/api/inner/motherlode/%s/mine", motherLodeId))
                .addParameter("unitsMined", unitsMined.toString())
                .build();

        String json = starloneRequestProvider.executeAndGetJson(request);
        try {
            return objectMapper.readValue(json, ResourceUnits.class);
        } catch (IOException e) {
            log.error("Unable to convert json {} to ResourceUnits class", json, e);
            throw new RuntimeException(e);
        }
    }

}
