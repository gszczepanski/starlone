package szczepanski.gerard.starlone.application.health;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * This is required singe free Heroku server is being off
 * after 30 minutes of inactivity.
 * <p>
 * Let's send awake request to app for each 20 min
 */
@Slf4j
@Component
class AwakeScheduler {

    @Value("${application.awake.url}")
    private String awakeURL;

    @Scheduled(cron = "0 0/20 * * * ?")
    void awakeApplication() throws IOException {
        log.debug("Sending awake request");

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(awakeURL);
        httpclient.execute(httpGet);
    }
}
