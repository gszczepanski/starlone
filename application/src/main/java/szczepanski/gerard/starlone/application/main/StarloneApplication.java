package szczepanski.gerard.starlone.application.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import szczepanski.gerard.starlone.building.BuildingConfiguration;
import szczepanski.gerard.starlone.commons.admin.AdminConfiguration;
import szczepanski.gerard.starlone.commons.applicationcall.ApplicationCallConfiguration;
import szczepanski.gerard.starlone.commons.events.EventsConfiguration;
import szczepanski.gerard.starlone.galaxy.GalaxyConfiguration;
import szczepanski.gerard.starlone.user.UserConfiguration;

@SpringBootApplication
@EnableScheduling
@Import({
        AdminConfiguration.class,
        ApplicationCallConfiguration.class,
        EventsConfiguration.class,
        UserConfiguration.class,
        GalaxyConfiguration.class,
        BuildingConfiguration.class
})
@ComponentScan("szczepanski.gerard.starlone.application.health")
public class StarloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarloneApplication.class, args);
    }
}
