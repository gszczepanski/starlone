package szczepanski.gerard.starlone.application.health;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("v0/api/app")
public class AwakeController {

    @GetMapping("/awake")
    String awake() {
        log.debug("Awake application");
        return "OK";
    }
}
