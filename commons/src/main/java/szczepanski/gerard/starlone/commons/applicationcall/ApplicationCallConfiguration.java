package szczepanski.gerard.starlone.commons.applicationcall;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "szczepanski.gerard.starlone.commons.applicationcall")
public class ApplicationCallConfiguration {

}
