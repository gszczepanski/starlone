package szczepanski.gerard.starlone.commons.events;

import lombok.Value;
import szczepanski.gerard.starlone.commons.id.DomainId;

@Value
public class UserCreatedEvent {

    private final DomainId id;

}
