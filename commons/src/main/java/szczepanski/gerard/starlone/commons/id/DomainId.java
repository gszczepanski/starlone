package szczepanski.gerard.starlone.commons.id;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static lombok.AccessLevel.PRIVATE;

@Value
@RequiredArgsConstructor(access = PRIVATE)
public class DomainId {

    private final String value;

    public static DomainId of(String id) {
        checkNotNull(id);
        checkState(!id.equals(""), "Id can not be empty!");
        return new DomainId(id);
    }

    public static DomainId create() {
        return DomainId.of(UUID.randomUUID().toString());
    }

    public DomainId() {
        this.value = null;
    }

    @Override
    public String toString() {
        return value;
    }

}
