package szczepanski.gerard.starlone.commons.id;

import javax.persistence.AttributeConverter;

public class DomainIdConverter implements AttributeConverter<DomainId, String> {

    @Override
    public String convertToDatabaseColumn(DomainId id) {
        if (id == null) {
            return null;
        }
        return id.toString();
    }

    @Override
    public DomainId convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        return DomainId.of(s);
    }
}


