package szczepanski.gerard.starlone.commons.applicationcall;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class StarloneRequestProvider {

    @Value("${application.call.auth}")
    private String authToken;

    @Value("${application.url}")
    private String url;

    public RequestBuilder getRequest(String path) {
        return addAppToken(RequestBuilder
                .get(url + path)
        );
    }

    public RequestBuilder putRequest(String path) {
        return addAppToken(RequestBuilder
                .put(url + path)
        );
    }

    private RequestBuilder addAppToken(RequestBuilder builder) {
        return builder
                .addHeader("Cookie", "starlone-app-token=" + authToken);
    }

    public String executeAndGetJson(HttpUriRequest request) {
        return extractJsonFromResponse(execute(request));
    }

    public HttpResponse execute(HttpUriRequest request) {
        HttpClient httpclient = HttpClients.createDefault();
        try {
            return httpclient.execute(request);
        } catch (IOException e) {
            log.error("HttpClient request error", e);
            throw new RuntimeException(e);
        }
    }

    public String extractJsonFromResponse(HttpResponse response) {
        try {
            return EntityUtils.toString(
                    response.getEntity()
            );
        } catch (IOException e) {
            log.error("Unable to extract JSON from response", e);
            throw new RuntimeException(e);
        }
    }
}
