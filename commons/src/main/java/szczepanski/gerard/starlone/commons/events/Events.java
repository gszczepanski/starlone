package szczepanski.gerard.starlone.commons.events;

import com.google.common.eventbus.EventBus;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This components is responsible for sending events.
 * Events are being send across app memory.
 * Events should be send only for eventual consistency actions (when we do not expect immediate results.
 * Events are being send on async thread pool.
 */
@Component
public class Events {

    private final EventBus eventBus;
    private final TaskExecutor executor;

    public Events(TaskExecutor executor) {
        this.eventBus = new EventBus();
        this.executor = executor;
    }

    public void publish(Object event) {
        checkNotNull(event);
        executor.execute(() -> eventBus.post(event));
    }

    public void register(Object subscriber) {
        checkNotNull(subscriber);
        eventBus.register(subscriber);
    }

}
