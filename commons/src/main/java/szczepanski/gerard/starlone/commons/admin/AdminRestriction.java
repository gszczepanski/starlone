package szczepanski.gerard.starlone.commons.admin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AdminRestriction {

    @Value("${admin.auth}")
    private String adminAuth;

    public void authorizeAdmin(String authToken) {
        if (!adminAuth.equals(authToken)) {
            throw new RuntimeException("UNAUTHORIZED");
        }
    }

}
