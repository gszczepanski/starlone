package szczepanski.gerard.starlone.commons.applicationcall;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationCallRestriction {

    @Value("${application.call.auth}")
    private String appCallAuth;

    public void authorizeApplicationRequest(String authToken) {
        if (!appCallAuth.equals(authToken)) {
            throw new RuntimeException("UNAUTHORIZED");
        }
    }

}
