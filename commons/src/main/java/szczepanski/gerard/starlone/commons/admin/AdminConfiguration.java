package szczepanski.gerard.starlone.commons.admin;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "szczepanski.gerard.starlone.commons.admin")
public class AdminConfiguration {
}
