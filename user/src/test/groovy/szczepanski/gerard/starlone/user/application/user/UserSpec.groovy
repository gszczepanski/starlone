package szczepanski.gerard.starlone.user.application.user

import spock.lang.Specification

class UserSpec extends Specification {

    def "should create user with given creds"() {
        given:
        CreateUserData data = new CreateUserData("testUser", "hello", "hello")

        when:
        User user = new User(data)

        then:
        user.credentials.username == "testUser" &&
                user.credentials.password != "hello"
    }

    def "should create user with domain id"() {
        given:
        CreateUserData data = new CreateUserData("testUser", "hello", "hello")

        when:
        User user = new User(data)

        then:
        user.domainId.value != null && user.domainId.value != ""
    }

    def "should throw exception if login is null during creation"() {
        when:
        new User(new CreateUserData(null, "hello", "hello"))

        then:
        thrown IllegalStateException
    }

    def "should throw exception if login is blank during creation"() {
        when:
        new User(new CreateUserData("", "hello", "hello"))

        then:
        thrown IllegalStateException
    }

    def "should throw exception if password is null during creation"() {
        when:
        new User(new CreateUserData("user", null, "hello"))

        then:
        thrown IllegalStateException
    }

    def "should throw exception if password is blank during creation"() {
        when:
        new User(new CreateUserData("user", "", "hello"))

        then:
        thrown IllegalStateException
    }

    def "should throw exception if repeated password does not match during creation"() {
        when:
        new User(new CreateUserData("user", "world", "hello"))

        then:
        thrown IllegalStateException
    }
}
