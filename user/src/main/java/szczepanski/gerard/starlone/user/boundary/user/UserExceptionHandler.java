package szczepanski.gerard.starlone.user.boundary.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import szczepanski.gerard.starlone.user.application.user.AuthException;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Slf4j
@ControllerAdvice
class UserExceptionHandler {

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(code = UNAUTHORIZED)
    public void authenticationFailure(AuthException e) {
        log.error("Authentication failure: {}", e.getMessage());
    }

}
