package szczepanski.gerard.starlone.user.boundary.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import szczepanski.gerard.starlone.user.application.user.UserService;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("v0/api/user")
@RequiredArgsConstructor
class UserController {

    private final UserService userService;

    @PostMapping(consumes = "application/json", produces = "application/json")
    String createUser(@RequestBody @Valid UserCreationRequest request) {
        log.debug("Create new user with username: {}", request.getUsername());
        return userService.createUser(request.assemble()).toString();
    }

}
