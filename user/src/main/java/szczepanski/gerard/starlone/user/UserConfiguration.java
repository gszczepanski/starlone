package szczepanski.gerard.starlone.user;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(value = "szczepanski.gerard.starlone.user")
@EnableJpaRepositories(basePackages = "szczepanski.gerard.starlone.user.infrastructure.jpa", considerNestedRepositories = true)
@EntityScan(basePackages = "szczepanski.gerard.starlone.user.application")
public class UserConfiguration {
}
