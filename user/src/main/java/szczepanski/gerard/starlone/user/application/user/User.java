package szczepanski.gerard.starlone.user.application.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import szczepanski.gerard.starlone.commons.id.DomainId;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Entity
@Table(schema = "USERS", name = "STARLONE_USER")
@NoArgsConstructor
public class User implements Serializable {

    @Id
    private String id;

    @Getter
    @Column(name = "CREATION_TIME")
    private LocalDateTime creationTime;

    @Getter
    @Embedded
    private Credentials credentials;

    User(CreateUserData data) {
        this.id = DomainId.create().toString();
        this.creationTime = LocalDateTime.now();
        this.credentials = Credentials.create(data);
    }

    public DomainId getDomainId() {
        return DomainId.of(id);
    }

    public void login(String pass) {
        checkNotNull(pass, "Password can not be null!");
        this.credentials.login(pass);
    }

    @NoArgsConstructor(access = PRIVATE)
    @AllArgsConstructor(access = PRIVATE)
    static class Credentials {

        @Column(name = "USERNAME", unique = true)
        private String username;

        @Column(name = "PASS")
        private String password;

        static Credentials create(CreateUserData data) {
            checkState(!isEmpty(data.getUsername()), "Username can not be empty!");
            checkState(!isEmpty(data.getPass()), "Password can not be empty!");

            guardRepeatedPasswordMatch(data);
            return new Credentials(data.getUsername(), hashPass(data.getPass()));
        }

        private static void guardRepeatedPasswordMatch(CreateUserData data) {
            if (!StringUtils.equals(data.getPass(), data.getRepeatPass())) {
                throw new IllegalStateException("Repeated password do not match!");
            }
        }

        private static String hashPass(String pass) {
            return PassHasher.enctyptPass(pass);
        }

        private void login(String pass) {
            if (!PassHasher.passCorrect(pass, password)) {
                throw new AuthException("Given credentials are incorrect");
            }
        }

    }

}
