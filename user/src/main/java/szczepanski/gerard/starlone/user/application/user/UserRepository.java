package szczepanski.gerard.starlone.user.application.user;

public interface UserRepository {

    void create(User user);

}
