package szczepanski.gerard.starlone.user.infrastructure;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.commons.events.Events;
import szczepanski.gerard.starlone.commons.events.UserCreatedEvent;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.user.application.user.UserEvents;

@Slf4j
@Component
@RequiredArgsConstructor
class UserEventsAdapter implements UserEvents {

    private final Events events;

    @Override
    public void userCreated(DomainId userId) {
        log.debug("Publish UserCreatedEvent with id {}", userId);
        events.publish(new UserCreatedEvent(userId));
    }

}
