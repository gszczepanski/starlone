package szczepanski.gerard.starlone.user.application.user;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;

class PassHasher {
    private static final int ITERATIONS = 20 * 1000;
    private static final int SALT_LEN = 32;
    private static final int DESIRED_KEY_LEN = 256;

    static String enctyptPass(String pass) {
        try {
            return getSaltedHash(pass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Computes a salted PBKDF2 hash of given plaintext password suitable for
     * storing in a database. Empty passwords are not supported.
     */
    private static String getSaltedHash(String pass) throws Exception {
        byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(SALT_LEN);
        // store the salt with the password
        return Base64.encodeBase64String(salt) + "$" + hash(pass, salt);
    }

    private static String hash(String pass, byte[] salt) throws Exception {
        if (pass == null || pass.length() == 0)
            throw new IllegalArgumentException("Empty passwords are not supported.");
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey key = f.generateSecret(new PBEKeySpec(pass.toCharArray(), salt, ITERATIONS, DESIRED_KEY_LEN));
        return Base64.encodeBase64String(key.getEncoded());
    }

    static Boolean passCorrect(String pass, String stored) {
        try {
            return arePasswordsTheSame(pass, stored);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean arePasswordsTheSame(String pass, String stored) throws Exception{
        String[] saltAndPass = stored.split("\\$");
        if (saltAndPass.length != 2) {
            throw new IllegalStateException(
                    "The stored password have the form 'salt$hash'");
        }
        String hashOfInput = hash(pass, Base64.decodeBase64(saltAndPass[0]));
        return hashOfInput.equals(saltAndPass[1]);
    }

}
