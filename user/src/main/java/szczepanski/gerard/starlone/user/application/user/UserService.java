package szczepanski.gerard.starlone.user.application.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import szczepanski.gerard.starlone.commons.id.DomainId;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;
    private final UserEvents events;

    @Transactional
    public DomainId createUser(CreateUserData data) {
        User user = new User(data);
        repository.create(user);

        DomainId domainId = user.getDomainId();
        events.userCreated(domainId);
        return domainId;
    }

}
