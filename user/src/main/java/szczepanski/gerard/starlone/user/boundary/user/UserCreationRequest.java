package szczepanski.gerard.starlone.user.boundary.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;
import szczepanski.gerard.starlone.user.application.user.CreateUserData;

@Getter
@NoArgsConstructor
@AllArgsConstructor
class UserCreationRequest {

    @NonNull
    private String username;

    @NonNull
    @Length(min = 6)
    private String pass;

    @NonNull
    @Length(min = 6)
    private String repeatPass;

    CreateUserData assemble() {
        return new CreateUserData(username, pass, repeatPass);
    }

}
