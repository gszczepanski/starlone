package szczepanski.gerard.starlone.user.application.user;

import lombok.Value;

@Value
public class CreateUserData {

    private final String username;
    private final String pass;
    private final String repeatPass;

}
