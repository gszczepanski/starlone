package szczepanski.gerard.starlone.user.application.user;

public class AuthException extends RuntimeException {

    public AuthException(String message) {
        super(message);
    }
}
