package szczepanski.gerard.starlone.user.application.user;

import szczepanski.gerard.starlone.commons.id.DomainId;

public interface UserEvents {

    void userCreated(DomainId userId);

}
