package szczepanski.gerard.starlone.user.infrastructure.jpa;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import szczepanski.gerard.starlone.user.application.user.User;
import szczepanski.gerard.starlone.user.application.user.UserRepository;

@Repository
@RequiredArgsConstructor
class UserRepositoryAdapter implements UserRepository {

    private final UserJpaRepository jpaRepository;

    @Override
    public void create(User user) {
        jpaRepository.save(user);
    }

    @Repository
    interface UserJpaRepository extends JpaRepository<User, String> {

    }
}
