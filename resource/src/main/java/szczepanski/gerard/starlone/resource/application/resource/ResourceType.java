package szczepanski.gerard.starlone.resource.application.resource;

public enum ResourceType {

    CRYSTAL,
    METAL,
    NAPHTHA,
    ENERGY,
    CRYPTOBLOCKS;

}
