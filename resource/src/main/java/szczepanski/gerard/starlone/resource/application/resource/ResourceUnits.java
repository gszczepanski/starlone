package szczepanski.gerard.starlone.resource.application.resource;

import lombok.NoArgsConstructor;
import lombok.Value;

@Value
public class ResourceUnits {

    private ResourceType type;
    private Integer quantity;

}
