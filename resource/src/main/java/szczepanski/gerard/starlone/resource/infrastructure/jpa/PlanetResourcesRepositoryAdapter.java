package szczepanski.gerard.starlone.resource.infrastructure.jpa;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.resource.application.resource.PlanetResources;
import szczepanski.gerard.starlone.resource.application.resource.PlanetResourcesRepository;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@Repository
@RequiredArgsConstructor
class PlanetResourcesRepositoryAdapter implements PlanetResourcesRepository {

    private final PlanetResourcesJpaRepository jpaRepository;

    @Override
    public Optional<PlanetResources> findForPlanet(DomainId planetId) {
        checkNotNull(planetId);

        return jpaRepository.findByPlanetId(planetId);
    }

    @Override
    public void save(PlanetResources planetResources) {
        checkNotNull(planetResources);
        jpaRepository.save(planetResources);
    }

    @Repository
    interface PlanetResourcesJpaRepository extends JpaRepository<PlanetResources, String> {

        @Query("SELECT pr FROM PlanetResources pr WHERE pr.planetId = :planetId")
        Optional<PlanetResources> findByPlanetId(@Param("planetId") DomainId planetId);

    }

}
