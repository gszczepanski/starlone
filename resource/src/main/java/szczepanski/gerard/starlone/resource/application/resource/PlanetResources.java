package szczepanski.gerard.starlone.resource.application.resource;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;

import javax.persistence.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * PlanetResources - act like account for resources per Planet
 * <br><br>
 * - Holds the resource info per Planet
 * <br>
 * - Has logic for adding/extracting resource per planet
 * <br>
 * - During add operation this object ask storehouses in Building bounded context about their capacity (if they can store amount of resources added)
 * If no, then exception is being thrown and operation is not valid.
 * <br>
 * - After extract operation this clas is sending event to storehouse for remove extracted amount of resources from storehouses.
 */
@Entity
@Table(schema = "RESOURCE", name = "PLANET_RESOURCES")
@EqualsAndHashCode
public class PlanetResources {

    @Id
    private String id;

    @Embedded
    private AssignedPlanet planet;

    @Column(name = "CRYSTAL_QTY")
    private Integer crystalsQuantity;

    @Column(name = "METAL_QTY")
    private Integer metalQuantity;

    @Column(name = "NAPHTHA_QTY")
    private Integer naphthaQuantity;

    @Column(name = "ENERGY_QTY")
    private Integer energyQuantity;

    @Column(name = "CRYPTOBLOCKS_QTY")
    private Integer cryptoblocksQuantity;

    PlanetResources(PlanetInfo planetInfo) {
        this.id = DomainId.create().toString();
        this.planet = new AssignedPlanet(planetInfo);
        this.crystalsQuantity = 0;
        this.metalQuantity = 0;
        this.naphthaQuantity = 0;
        this.energyQuantity = 0;
        this.cryptoblocksQuantity = 0;
    }

    void update(ResourceUpdateData data, StorehouseClient storehouse) {
        checkNotNull(data);
        checkState(id.equals(data.getPlanetId().toString()), "Wrong planetId for resources!");



    }

    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public class AssignedPlanet {

        @Column(name = "PLANET_ID")
        @Convert(converter = DomainIdConverter.class)
        private DomainId galaxyId;

        @Column(name = "PLANET_ID")
        @Convert(converter = DomainIdConverter.class)
        private DomainId planetId;

        private AssignedPlanet(PlanetInfo planetInfo) {
            checkNotNull(planetInfo);
            checkNotNull(planetInfo.getGalaxyId());
            checkNotNull(planetInfo.getPlanetId());

            this.galaxyId = planetInfo.getGalaxyId();
            this.planetId = planetInfo.getPlanetId();
        }

    }

}
