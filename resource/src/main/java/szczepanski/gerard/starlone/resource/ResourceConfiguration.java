package szczepanski.gerard.starlone.resource;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(value = "szczepanski.gerard.resource")
@EnableJpaRepositories(basePackages = "szczepanski.gerard.starlone.resource.infrastructure.jpa", considerNestedRepositories = true)
@EntityScan(basePackages = "szczepanski.gerard.starlone.resource.application")
public class ResourceConfiguration {

}
