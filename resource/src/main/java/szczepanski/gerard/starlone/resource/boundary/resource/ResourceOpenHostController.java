package szczepanski.gerard.starlone.resource.boundary.resource;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.starlone.commons.applicationcall.ApplicationCallRestriction;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.resource.application.resource.PlanetResourcesService;

@Slf4j
@RestController
@RequestMapping("v0/api/inner/resource")
@RequiredArgsConstructor
public class ResourceOpenHostController {

    private final ApplicationCallRestriction restriction;
    private final PlanetResourcesService planetResourcesService;

    @ModelAttribute
    void applicationCallRestriction(@CookieValue("starlone-app-token") String appToken) {
        restriction.authorizeApplicationRequest(appToken);
    }

    @PutMapping(value = "/planet/{planetId}", consumes = "application/json")
    void updateResourcesStoredForPlanet(@PathVariable("planetId") DomainId planetId, @RequestBody UpdatePlanetResourcesRequest request) {
        log.debug("Update planet {} with request: {}", planetId, request);
        planetResourcesService.resourcesOperation(
                request.assembly(planetId)
        );
    }


}
