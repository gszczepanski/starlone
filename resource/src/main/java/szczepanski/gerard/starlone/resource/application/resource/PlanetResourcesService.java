package szczepanski.gerard.starlone.resource.application.resource;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlanetResourcesService {

    private final PlanetResourcesRepository repository;
    private final PlanetResourcesFactory factory;
    private final StorehouseClient storehouseClient;

    @Transactional
    public void resourcesOperation(ResourceUpdateData data) {
        DomainId planetId = data.getPlanetId();
        Optional<PlanetResources> optionalPlanetResources = repository.findForPlanet(planetId);

        if (optionalPlanetResources.isPresent()) {
            PlanetResources planetResources = optionalPlanetResources.get();
            planetResources.update(data, storehouseClient);
        } else {
            createPlanetResourcesForPlanet(planetId);
            resourcesOperation(data);
        }
    }

    private void createPlanetResourcesForPlanet(DomainId planetId) {
        log.debug("Create new PlanetResources for planet {}", planetId);
        PlanetResources planetResources = factory.createForPlanet(planetId);
        repository.save(planetResources);
    }

}
