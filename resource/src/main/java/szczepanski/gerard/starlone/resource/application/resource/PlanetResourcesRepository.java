package szczepanski.gerard.starlone.resource.application.resource;

import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Optional;

public interface PlanetResourcesRepository {

    Optional<PlanetResources> findForPlanet(DomainId planetId);

    void save(PlanetResources planetResources);

}
