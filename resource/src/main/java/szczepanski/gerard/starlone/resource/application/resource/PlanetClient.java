package szczepanski.gerard.starlone.resource.application.resource;

import szczepanski.gerard.starlone.commons.id.DomainId;

public interface PlanetClient {

    PlanetInfo findPlanet(DomainId planetId);

}
