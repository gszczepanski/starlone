package szczepanski.gerard.starlone.resource.application.resource;

import lombok.Value;
import szczepanski.gerard.starlone.commons.id.DomainId;

@Value
public class PlanetInfo {

    private DomainId galaxyId;
    private DomainId planetId;

}
