package szczepanski.gerard.starlone.resource.boundary.resource;

import lombok.NoArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.resource.application.resource.ResourceOperationType;
import szczepanski.gerard.starlone.resource.application.resource.ResourceUnits;
import szczepanski.gerard.starlone.resource.application.resource.ResourceUpdateData;

import java.util.List;

@NoArgsConstructor
class UpdatePlanetResourcesRequest {

    private List<ResourceUnits> resourceUnits;
    private ResourceOperationType operationType;

    ResourceUpdateData assembly(DomainId planetId) {
        return ResourceUpdateData.builder()
                .planetId(planetId)
                .resourceUnits(resourceUnits)
                .operationType(operationType)
                .build();
    }

}
