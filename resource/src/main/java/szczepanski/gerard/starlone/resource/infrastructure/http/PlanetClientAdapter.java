package szczepanski.gerard.starlone.resource.infrastructure.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.commons.applicationcall.StarloneRequestProvider;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.resource.application.resource.PlanetClient;
import szczepanski.gerard.starlone.resource.application.resource.PlanetInfo;

import java.io.IOException;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
class PlanetClientAdapter implements PlanetClient {

    private final StarloneRequestProvider starloneRequestProvider;
    private final ObjectMapper objectMapper;

    @Override
    public PlanetInfo findPlanet(DomainId planetId) {
        log.debug("Sending planet info request with planetId {}", planetId);

        HttpUriRequest request = starloneRequestProvider
                .getRequest(format("/v0/api/inner/planet/%s", planetId))
                .build();

        HttpResponse httpResponse = starloneRequestProvider.execute(request);
        return extractPlanetInfoFromResponse(planetId, httpResponse);
    }

    private PlanetInfo extractPlanetInfoFromResponse(DomainId planetId, HttpResponse response) {
        if (planetNotFound(response)) {
            throw new RuntimeException(format("Planet with id %s not found!", planetId));
        }

        String json = starloneRequestProvider.extractJsonFromResponse(response);
        try {
            return objectMapper.readValue(json, PlanetInfo.class);
        } catch (IOException e) {
            log.error("Unable to convert JSON {}", json, e);
            throw new RuntimeException(e);
        }
    }

    private boolean planetNotFound(HttpResponse response) {
        return response.getStatusLine().getStatusCode() == 404;
    }
}
