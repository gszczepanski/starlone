package szczepanski.gerard.starlone.resource.application.resource;

import lombok.Builder;
import lombok.Value;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.List;

@Value
@Builder
public class ResourceUpdateData {

    private final DomainId planetId;
    private final List<ResourceUnits> resourceUnits;
    private final ResourceOperationType operationType;

}
