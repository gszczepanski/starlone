package szczepanski.gerard.starlone.resource.application.resource;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.commons.id.DomainId;

import static com.google.common.base.Preconditions.checkNotNull;

@Component
@RequiredArgsConstructor
class PlanetResourcesFactory {

    private final PlanetClient planetClient;

    PlanetResources createForPlanet(DomainId planetId) {
        checkNotNull(planetId);

        PlanetInfo planet = planetClient.findPlanet(planetId);
        return new PlanetResources(planet);
    }

}
