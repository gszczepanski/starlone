# starlone
Simple app (like ogame) for Heroku server sandbox testing.

# Intention
MMO browser game for testing Heroku server. This app is just my playground for training Domain Driven Design, testing and hexagonal architecture.

# About
Whole app runs as single process. However each module is treated like single application.<br>
Thanks to this I can separate parts of my app gracefuly and make them independend from each other (avoiding circular dependencies or big services for everything). If modules will grown in the future, I can easly split them info independent microservices.
<br><br>
Application exposes REST API.
Front end will be created as different application on different server.

# Modules
By now I have:
- User - Bounded context responsible for create/update user in system
- Galaxy - module responsible for create galaxy for some number of players
- Building - module responsible for buildings on each planet

Each module is created following ports and adapters architecture. <br><br>
Modules communicate with each other two ways:
- If there is no need to do some work in one transaction, then I use in memory EventBus from guava.
Events are not persisted now.
- If there is a need to do some work in one transaction, then modules communicate with each other through HTTP following
Open Host pattern from Vernon DDD book. Inner app calls have secret auth cookie. 

# CQRS
Each module contains domain objects with logic. <br>
Entities designed for read only, are named with DTO suffix and lying under read package in application package.
