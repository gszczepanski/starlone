package szczepanski.gerard.starlone.galaxy;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(value = "szczepanski.gerard.starlone.galaxy")
@EnableJpaRepositories(basePackages = "szczepanski.gerard.starlone.galaxy.infrastructure.jpa", considerNestedRepositories = true)
@EntityScan(basePackages = "szczepanski.gerard.starlone.galaxy.application")
public class GalaxyConfiguration {

}
