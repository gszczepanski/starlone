package szczepanski.gerard.starlone.galaxy.application.galaxy.read;

import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Optional;
import java.util.Set;

public interface GalaxyReadRepository {

    Set<MotherLodeDto> findAllMotherLodesForPlanet(DomainId planetId);

    Optional<PlanetDto> findPlanet(DomainId planetId);

}
