package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.Value;

@Value
public class CreateGalaxyData {

    private final int width;
    private final int height;
    private final int numberOfPlanets;

}
