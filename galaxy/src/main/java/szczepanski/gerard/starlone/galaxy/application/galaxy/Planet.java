package szczepanski.gerard.starlone.galaxy.application.galaxy;

import com.google.common.base.Preconditions;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;

import javax.persistence.*;

import static com.google.common.base.Preconditions.*;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(schema = "GALAXY", name = "PLANET")
@NoArgsConstructor
@EqualsAndHashCode
public class Planet {

    @Id
    private String id;

    @Column(name = "USER_ID")
    @Convert(converter = DomainIdConverter.class)
    private DomainId ownerUserId;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "GALAXY_ID")
    private Galaxy galaxy;

    @Getter
    @Enumerated(value = STRING)
    @Column(name = "SIZE")
    private PlanetSize size;

    @Getter
    @Column(name = "COORDINATES")
    @Convert(converter = CoordinatesConverter.class)
    private Coordinates coordinates;

    Planet(Galaxy galaxy, PlanetSize size, Coordinates coordinates) {
        this.id = DomainId.create().toString();
        this.galaxy = galaxy;
        this.size = size;
        this.coordinates = coordinates;
    }

    public DomainId getDomainId() {
        return DomainId.of(id);
    }

    public void assignUser(DomainId userId) {
        checkNotNull(userId);
        this.ownerUserId = userId;
    }

    @Getter
    @RequiredArgsConstructor
    public enum PlanetSize {

        TINY(5),
        SMALL(10),
        MEDIUM(15),
        BIG(20),
        LARGE(25);

        private final Integer size;

    }
}
