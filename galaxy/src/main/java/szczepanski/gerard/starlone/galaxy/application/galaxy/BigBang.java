package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.awt.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Responsible for generate new Galaxy.
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class BigBang {

    private static final Random RANDOM = new Random();
    private static final int minDistanceBetweenPlanets = 20;

    private final Galaxy galaxy;
    private final Set<Planet> planets;
    private final Set<MotherLode> motherLodes;

    static BigBang create(CreateGalaxyData data) {
        Galaxy galaxy = new Galaxy(data.getWidth(), data.getHeight());
        Set<Planet> planets = planetsFor(galaxy, data);
        return new BigBang(galaxy, planets, motherLodesForPlanets(planets));
    }

    private static Set<Planet> planetsFor(Galaxy galaxy, CreateGalaxyData data) {
        Set<Planet> planets = new HashSet<>(data.getNumberOfPlanets());

        for (int i = 0; i < data.getNumberOfPlanets(); i++) {
            planets.add(planet(galaxy, planets));
        }
        return planets;
    }

    private static Planet planet(Galaxy galaxy, Set<Planet> otherPlanets) {
        Planet.PlanetSize size = planetSize();
        Coordinates coords = coords(galaxy, size, otherPlanets);

        return new Planet(galaxy, size, coords);
    }

    private static Planet.PlanetSize planetSize() {
        return Planet.PlanetSize.values()[RANDOM.nextInt(Planet.PlanetSize.values().length)];
    }

    private static Coordinates coords(Galaxy galaxy, Planet.PlanetSize size, Set<Planet> otherPlanets) {
        int x = RANDOM.nextInt(galaxy.getWidth());
        int y = RANDOM.nextInt(galaxy.getHeight());

        if (isInGalaxyBounds(galaxy, size, x, y) && doNotCollideWithOtherPlanets(size, x, y, otherPlanets)) {
            return new Coordinates(x, y);
        }
        return coords(galaxy, size, otherPlanets);
    }

    private static boolean isInGalaxyBounds(Galaxy galaxy, Planet.PlanetSize size, int x, int y) {
        return x + size.getSize() < galaxy.getWidth() && y + size.getSize() < galaxy.getHeight();
    }

    private static boolean doNotCollideWithOtherPlanets(Planet.PlanetSize size, int x, int y, Set<Planet> otherPlanets) {
        for (Planet otherPlanet : otherPlanets) {
            if (isPlanetCollision(size, x, y, otherPlanet)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isPlanetCollision(Planet.PlanetSize size, int x, int y, Planet other) {
        int planetSize = size.getSize() + minDistanceBetweenPlanets;
        Rectangle rectangle = new Rectangle(x - minDistanceBetweenPlanets, y - minDistanceBetweenPlanets, planetSize, planetSize);

        int otherSize = other.getSize().getSize();
        Rectangle otherPlanetRectangle = new Rectangle(other.getCoordinates().getX(), other.getCoordinates().getY(), otherSize, otherSize);

        return rectangle.getBounds().intersects(otherPlanetRectangle.getBounds());
    }

    private static Set<MotherLode> motherLodesForPlanets(Set<Planet> planets) {
        Set<MotherLode> motherLodes = new HashSet<>();

        for (Planet planet : planets) {
            motherLodes.addAll(motherLodesForPlanet(planet));
        }
        return motherLodes;
    }

    private static Set<MotherLode> motherLodesForPlanet(Planet planet) {
        Set<MotherLode> motherLodesForPlanet = new HashSet<>();
        for (int i = 0; i < planet.getSize().getSize(); i++) {
            motherLodesForPlanet.add(
                    new MotherLode(resourceType(), planet)
            );
        }
        return motherLodesForPlanet;
    }

    private static ResourceType resourceType() {
        return ResourceType.values()[RANDOM.nextInt(ResourceType.values().length)];
    }

}
