package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlanetService {

    private final GalaxyRepository galaxyRepository;
    private final GalaxyCreationService galaxyCreationService;
    private final GalaxyEvents events;

    @Transactional
    public void assignPlanetForNewUser(DomainId userId) {
        Optional<Planet> firstFreePlanet = galaxyRepository.findFirstFreePlanet();
        if (firstFreePlanet.isPresent()) {
            assignUserToPlanet(
                    firstFreePlanet.get(),
                    userId
            );
        } else {
            createNewGalaxyAndThenReassignPlanet(userId);
        }
    }

    private void createNewGalaxyAndThenReassignPlanet(DomainId userId) {
        log.debug("No more free planets. Creating new galaxy...");
        galaxyCreationService.createDefaultGalaxy();
        assignPlanetForNewUser(userId);
    }

    private void assignUserToPlanet(Planet planet, DomainId userId) {
        planet.assignUser(userId);
        galaxyRepository.updatePlanet(planet);
        events.planetAssignedToNewUser(planet.getDomainId());
    }

}
