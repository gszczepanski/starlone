package szczepanski.gerard.starlone.galaxy.application.galaxy;

import szczepanski.gerard.starlone.commons.id.DomainId;

public interface GalaxyEvents {

    void planetAssignedToNewUser(DomainId planetId);

}
