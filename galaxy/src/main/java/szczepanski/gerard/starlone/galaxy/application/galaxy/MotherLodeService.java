package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.galaxy.application.galaxy.MotherLode.UnitsMined;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MotherLodeService {

    private final GalaxyRepository galaxyRepository;

    @Transactional
    public UnitsMined mine(DomainId motherLodeId, Integer unitsMined) {
        Optional<MotherLode> motherLode = galaxyRepository.findMotherLodeById(motherLodeId);
        if (motherLode.isPresent()) {
            return motherLode.get()
                      .mine(unitsMined);
        }
        throw new RuntimeException("MotherLode can not be found!");
    }
}
