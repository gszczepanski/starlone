package szczepanski.gerard.starlone.galaxy.infrastructure;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.commons.events.Events;
import szczepanski.gerard.starlone.commons.events.PlanetAssignedToNewUserEvent;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.galaxy.application.galaxy.GalaxyEvents;

@Slf4j
@Component
@RequiredArgsConstructor
class GalaxyEventsAdapter implements GalaxyEvents {

    private final Events events;

    @Override
    public void planetAssignedToNewUser(DomainId planetId) {
        log.debug("Sending event: Planet assigned to new user with planetId {}", planetId);
        events.publish(new PlanetAssignedToNewUserEvent(planetId));
    }
}
