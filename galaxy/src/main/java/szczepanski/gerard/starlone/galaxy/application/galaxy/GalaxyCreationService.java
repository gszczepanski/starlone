package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class GalaxyCreationService {

    private final GalaxyProperties properties;
    private final GalaxyRepository galaxyWriteRepository;

    @Transactional
    public void createGalaxy(CreateGalaxyData data) {
        log.debug("Creating new Galaxy... {}", data);
        BigBang bigBang = BigBang.create(data);

        galaxyWriteRepository.createGalaxy(bigBang.getGalaxy());
        galaxyWriteRepository.createPlanets(bigBang.getPlanets());
        galaxyWriteRepository.createMotherLodes(bigBang.getMotherLodes());

        log.debug("Galaxy successfully created");
    }

    @Transactional
    public void createDefaultGalaxy() {
        createGalaxy(new CreateGalaxyData(
                properties.defGalaxyWidth(),
                properties.defGalaxyHeight(),
                properties.defGalaxyNumberOfPlanets()
        ));
    }

}
