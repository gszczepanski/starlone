package szczepanski.gerard.starlone.galaxy.boundary.galaxy;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.starlone.commons.applicationcall.ApplicationCallRestriction;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.galaxy.application.galaxy.MotherLodeService;

import static szczepanski.gerard.starlone.galaxy.application.galaxy.MotherLode.*;

/**
 * This is open host controller for MotherLode requests in Galaxy bounded context.
 * Destination of this controller is to produce responses to other application components.
 */
@Slf4j
@RestController
@RequestMapping("v0/api/inner/motherlode")
@RequiredArgsConstructor
class MotherLodeOpenHostController {

    private final ApplicationCallRestriction restriction;
    private final MotherLodeService motherLodeService;
    @ModelAttribute
    void applicationCallRestriction(@CookieValue("starlone-app-token") String appToken) {
        restriction.authorizeApplicationRequest(appToken);
    }

    @PutMapping(value = "/{motherLodeId}/mine", produces = "application/json")
    UnitsMined mineMotherlode(@PathVariable DomainId motherLodeId, @RequestParam Integer unitsMined) {
        log.debug("Mine units {} for motherlode {}", unitsMined, motherLodeId);
        return motherLodeService.mine(motherLodeId, unitsMined);
    }

}
