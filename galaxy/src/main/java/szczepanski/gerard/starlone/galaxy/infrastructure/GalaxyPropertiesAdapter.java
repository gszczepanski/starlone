package szczepanski.gerard.starlone.galaxy.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.galaxy.application.galaxy.GalaxyProperties;

@Component
class GalaxyPropertiesAdapter implements GalaxyProperties {

    @Value("${starlone.default.galaxy.width}")
    private Integer defGalaxyWidth;

    @Value("${starlone.default.galaxy.height}")
    private Integer defGalaxyHeight;

    @Value("${starlone.default.galaxy.no.of.planets}")
    private Integer defGalaxyNumberOfPlanets;

    @Override
    public int defGalaxyWidth() {
        return defGalaxyWidth;
    }

    @Override
    public int defGalaxyHeight() {
        return defGalaxyHeight;
    }

    @Override
    public int defGalaxyNumberOfPlanets() {
        return defGalaxyNumberOfPlanets;
    }
}
