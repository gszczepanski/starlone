package szczepanski.gerard.starlone.galaxy.boundary.galaxy;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import szczepanski.gerard.starlone.commons.events.Events;
import szczepanski.gerard.starlone.commons.events.UserCreatedEvent;
import szczepanski.gerard.starlone.galaxy.application.galaxy.PlanetService;

@Slf4j
@Component
class GalaxyEventListener {

    private final PlanetService planetService;

    GalaxyEventListener(PlanetService galaxyAssign, Events events) {
        this.planetService = galaxyAssign;
        events.register(this);
    }

    @Subscribe
    void onUserCreation(UserCreatedEvent event) {
        log.debug("Start assigning galaxy for created user with id: {}", event.getId());
        planetService.assignPlanetForNewUser(event.getId());
    }

}
