package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(schema = "GALAXY", name = "GALAXY")
@NoArgsConstructor
public class Galaxy {

    @Id
    private String id;

    @Getter
    @Column(name = "WIDTH")
    private Integer width;

    @Getter
    @Column(name = "HEIGHT")
    private Integer height;

    @Column(name = "CREATION_TIME")
    private LocalDateTime creationTime;

    Galaxy(Integer width, Integer height) {
        this.id = DomainId.create().toString();
        this.creationTime = LocalDateTime.now();
        this.width = width;
        this.height = height;
    }

    public DomainId getDomainId() {
        return DomainId.of(id);
    }

}
