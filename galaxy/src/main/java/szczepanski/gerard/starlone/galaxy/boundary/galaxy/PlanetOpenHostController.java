package szczepanski.gerard.starlone.galaxy.boundary.galaxy;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.starlone.commons.applicationcall.ApplicationCallRestriction;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.galaxy.application.galaxy.read.GalaxyReadRepository;
import szczepanski.gerard.starlone.galaxy.application.galaxy.read.MotherLodeDto;
import szczepanski.gerard.starlone.galaxy.application.galaxy.read.PlanetDto;

import java.util.Optional;
import java.util.Set;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * This is open host controller for Planet requests in Galaxy bounded context.
 * Destination of this controller is to produce responses to other application components.
 */
@Slf4j
@RestController
@RequestMapping("v0/api/inner/planet")
@RequiredArgsConstructor
class PlanetOpenHostController {

    private final ApplicationCallRestriction restriction;
    private final GalaxyReadRepository galaxyReadRepository;

    @ModelAttribute
    void applicationCallRestriction(@CookieValue("starlone-app-token") String appToken) {
        restriction.authorizeApplicationRequest(appToken);
    }

    @GetMapping(value = "/{planetId}", produces = "application/json")
    ResponseEntity<PlanetDto> getPlanetInfo(@PathVariable DomainId planetId) {
        log.debug("Fetch planet {} info", planetId);

        Optional<PlanetDto> optionalPlanet = galaxyReadRepository.findPlanet(planetId);
        if (optionalPlanet.isPresent()) {
            return new ResponseEntity<>(optionalPlanet.get(), OK);
        }

        log.debug("Planet with id {} not found", planetId);
        return new ResponseEntity<>(NOT_FOUND);
    }

    @GetMapping(value = "/{planetId}/motherlode", produces = "application/json")
    Set<MotherLodeDto> fetchMotherLodesForPlanet(@PathVariable DomainId planetId) {
        log.debug("Fetch motherlodes for planet with id: {}", planetId);
        return galaxyReadRepository.findAllMotherLodesForPlanet(planetId);
    }

}
