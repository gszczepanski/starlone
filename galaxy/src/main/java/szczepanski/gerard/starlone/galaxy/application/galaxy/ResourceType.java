package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.RequiredArgsConstructor;

import java.util.Random;

@RequiredArgsConstructor
public enum ResourceType {

    CRYSTAL(300, 2000),
    METAL(2500, 10000),
    NAPHTHA(1000, 15000),
    ENERGY(2000, 8000),
    CRYPTOBLOCKS(1200, 10000);

    private final Integer minUnitsPerPlanet;
    private final Integer maxUnitsPerPlanet;

    Integer countUnitsPerPlanet() {
        return new Random().nextInt(maxUnitsPerPlanet - minUnitsPerPlanet + 1) + minUnitsPerPlanet;
    }

}
