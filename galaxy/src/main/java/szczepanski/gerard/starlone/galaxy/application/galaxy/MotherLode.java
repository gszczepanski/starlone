package szczepanski.gerard.starlone.galaxy.application.galaxy;

import com.google.common.base.Preconditions;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Value;
import szczepanski.gerard.starlone.commons.id.DomainId;

import javax.persistence.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

/**
 * Represents motherlode in game
 * Has one resource type, maximum units of that resource and info aboutr already extracted units.
 *
 * Can be placed only at one planet.
 */

@Entity
@Table(schema = "GALAXY", name = "MOTHER_LODE")
@NoArgsConstructor
@EqualsAndHashCode
public class MotherLode {

    @Id
    private String id;

    @Enumerated(value = STRING)
    @Column(name = "RESOURCE_TYPE")
    private ResourceType resourceType;

    @Column(name = "MAX_UNITS")
    private Integer maximumUnits;

    @Column(name = "EXTRACTED_UNITS")
    private Integer extractedUnits;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "PLANET_ID")
    private Planet planet;

    MotherLode(ResourceType resourceType, Planet planet) {
        this.id = DomainId.create().toString();
        this.resourceType = resourceType;
        this.maximumUnits = resourceType.countUnitsPerPlanet();
        this.extractedUnits = 0;
        this.planet = planet;
    }

    // TODO gszczepanski future refactor
    UnitsMined mine(Integer unitsMined) {
        checkNotNull(unitsMined);

        int unitsLeft = maximumUnits - extractedUnits;
        if (unitsLeft > 0) {
            if (unitsLeft > unitsMined) {
                extractedUnits += unitsMined;
                return new UnitsMined(resourceType, unitsMined);
            } else {
                extractedUnits = 0;
                return new UnitsMined(resourceType, unitsLeft);
            }
        }
        return new UnitsMined(resourceType, 0);
    }

    @Value
    public static class UnitsMined {

        private final ResourceType type;
        private final Integer quantity;

    }

}
