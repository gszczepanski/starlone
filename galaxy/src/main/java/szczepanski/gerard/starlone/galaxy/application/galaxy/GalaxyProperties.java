package szczepanski.gerard.starlone.galaxy.application.galaxy;

public interface GalaxyProperties {

    int defGalaxyWidth();

    int defGalaxyHeight();

    int defGalaxyNumberOfPlanets();

}
