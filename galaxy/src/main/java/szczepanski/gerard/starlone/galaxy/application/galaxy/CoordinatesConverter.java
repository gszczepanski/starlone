package szczepanski.gerard.starlone.galaxy.application.galaxy;

import javax.persistence.AttributeConverter;

import static java.lang.Integer.valueOf;

public class CoordinatesConverter implements AttributeConverter<Coordinates, String> {

    @Override
    public String convertToDatabaseColumn(Coordinates coordinates) {
        return coordinates.getX() + ":" + coordinates.getY();
    }

    @Override
    public Coordinates convertToEntityAttribute(String s) {
        String[] coordinates = s.split(":");
        return new Coordinates(valueOf(coordinates[0]), valueOf(coordinates[1]));
    }
}


