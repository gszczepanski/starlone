package szczepanski.gerard.starlone.galaxy.application.galaxy;

import szczepanski.gerard.starlone.commons.id.DomainId;

import java.util.Optional;
import java.util.Set;

public interface GalaxyRepository {

    void createGalaxy(Galaxy galaxy);

    void createPlanets(Set<Planet> planets);

    void createMotherLodes(Set<MotherLode> motherLodes);

    void updatePlanet(Planet planet);

    Optional<Planet> findFirstFreePlanet();

    Optional<MotherLode> findMotherLodeById(DomainId motherLodeId);

}
