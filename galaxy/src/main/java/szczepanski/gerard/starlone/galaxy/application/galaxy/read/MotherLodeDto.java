package szczepanski.gerard.starlone.galaxy.application.galaxy.read;

import jdk.nashorn.internal.ir.annotations.Immutable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;

import javax.persistence.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Entity
@Table(schema = "GALAXY", name = "MOTHER_LODE")
@EqualsAndHashCode
@Immutable
@NoArgsConstructor(access = PRIVATE)
public class MotherLodeDto {

    @Id
    private String id;

    @Column(name = "RESOURCE_TYPE")
    private String resourceType;

    @Column(name = "PLANET_ID")
    @Convert(converter = DomainIdConverter.class)
    private DomainId planetId;

}
