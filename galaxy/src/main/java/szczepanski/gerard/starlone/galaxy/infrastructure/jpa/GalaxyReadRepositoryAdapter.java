package szczepanski.gerard.starlone.galaxy.infrastructure.jpa;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.galaxy.application.galaxy.read.GalaxyReadRepository;
import szczepanski.gerard.starlone.galaxy.application.galaxy.read.MotherLodeDto;

import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

@Repository
@RequiredArgsConstructor
class GalaxyReadRepositoryAdapter implements GalaxyReadRepository {

    private final MotherLodeReadJpaRepository motherLodeReadJpaRepository;

    @Override
    public Set<MotherLodeDto> findAllMotherLodesForPlanet(DomainId planetId) {
        checkNotNull(planetId);
        return motherLodeReadJpaRepository.findAllMotherLodesForPlanet(planetId);
    }

    @Repository
    interface MotherLodeReadJpaRepository extends JpaRepository<MotherLodeDto, String> {
        @Query("SELECT m FROM MotherLodeDto m WHERE m.planetId = :planetId")
        Set<MotherLodeDto> findAllMotherLodesForPlanet(@Param("planetId") DomainId planetId);
    }
}
