package szczepanski.gerard.starlone.galaxy.application.galaxy;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Coordinates {

    private final int x;
    private final int y;

}
