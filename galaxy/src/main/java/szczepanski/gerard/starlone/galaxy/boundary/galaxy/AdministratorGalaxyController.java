package szczepanski.gerard.starlone.galaxy.boundary.galaxy;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.starlone.commons.admin.AdminRestriction;
import szczepanski.gerard.starlone.galaxy.application.galaxy.CreateGalaxyData;
import szczepanski.gerard.starlone.galaxy.application.galaxy.GalaxyCreationService;

@Slf4j
@RestController
@RequestMapping("v0/api/admin/galaxy")
@RequiredArgsConstructor
public class AdministratorGalaxyController {

    private final AdminRestriction adminRestriction;
    private final GalaxyCreationService galaxyCreationService;

    @ModelAttribute
    void adminRestriction(@CookieValue String adminToken) {
        adminRestriction.authorizeAdmin(adminToken);
    }

    @PostMapping(consumes = "application/json")
    void createGalaxy(@RequestBody CreateGalaxyRequest request) {
        log.debug("Create galaxy with data {}", request);
        galaxyCreationService.createGalaxy(request.assembly());
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    public static class CreateGalaxyRequest {

        private int width;
        private int height;
        private int numberOfPlanets;

        CreateGalaxyData assembly() {
            return new CreateGalaxyData(width, height, numberOfPlanets);
        }

    }

}
