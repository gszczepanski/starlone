package szczepanski.gerard.starlone.galaxy.infrastructure.jpa;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.galaxy.application.galaxy.Galaxy;
import szczepanski.gerard.starlone.galaxy.application.galaxy.GalaxyRepository;
import szczepanski.gerard.starlone.galaxy.application.galaxy.MotherLode;
import szczepanski.gerard.starlone.galaxy.application.galaxy.Planet;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Repository
@RequiredArgsConstructor
class GalaxyRepositoryAdapter implements GalaxyRepository {

    private final GalaxyJpaRepository galaxyJpaRepository;
    private final PlanetJpaRepository planetJpaRepository;
    private final MotherLodeJpaRepository motherLodeJpaRepository;

    @Override
    public void createGalaxy(Galaxy galaxy) {
        galaxyJpaRepository.save(galaxy);
    }

    @Override
    public void createPlanets(Set<Planet> planets) {
        planetJpaRepository.save(planets);
    }

    @Override
    public void createMotherLodes(Set<MotherLode> motherLodes) {
        motherLodeJpaRepository.save(motherLodes);
    }

    @Override
    public void updatePlanet(Planet planet) {
        planetJpaRepository.save(planet);
    }

    @Override
    public Optional<Planet> findFirstFreePlanet() {
        List<Planet> freePlanets = planetJpaRepository.findFreePlanets();
        if (!freePlanets.isEmpty()) {
            return of(freePlanets.get(0));
        }
        return empty();
    }

    @Override
    public Optional<MotherLode> findMotherLodeById(DomainId motherLodeId) {
        checkNotNull(motherLodeId);
        return motherLodeJpaRepository.findOptionalById(motherLodeId.toString());
    }

    @Repository
    interface GalaxyJpaRepository extends JpaRepository<Galaxy, String> {

    }

    @Repository
    interface PlanetJpaRepository extends JpaRepository<Planet, String> {
        @Query("SELECT p FROM Planet p WHERE p.ownerUserId IS NULL")
        List<Planet> findFreePlanets();
    }

    @Repository
    interface MotherLodeJpaRepository extends JpaRepository<MotherLode, String> {

        @Query("SELECT m FROM MotherLode m WHERE m.id = :id")
        Optional<MotherLode> findOptionalById(@Param("id") String id);
    }

}
