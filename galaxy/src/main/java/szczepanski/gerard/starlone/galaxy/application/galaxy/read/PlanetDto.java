package szczepanski.gerard.starlone.galaxy.application.galaxy.read;

import jdk.nashorn.internal.ir.annotations.Immutable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.starlone.commons.id.DomainId;
import szczepanski.gerard.starlone.commons.id.DomainIdConverter;
import szczepanski.gerard.starlone.galaxy.application.galaxy.Coordinates;
import szczepanski.gerard.starlone.galaxy.application.galaxy.CoordinatesConverter;
import szczepanski.gerard.starlone.galaxy.application.galaxy.Galaxy;
import szczepanski.gerard.starlone.galaxy.application.galaxy.Planet;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;
import static lombok.AccessLevel.PRIVATE;

@Getter
@Entity
@Table(schema = "GALAXY", name = "PLANET")
@EqualsAndHashCode
@Immutable
@NoArgsConstructor(access = PRIVATE)
public class PlanetDto {

    @Id
    private String id;

    @Column(name = "USER_ID")
    @Convert(converter = DomainIdConverter.class)
    private DomainId ownerUserId;

    @Column(name = "GALAXY_ID")
    @Convert(converter = DomainIdConverter.class)
    private DomainId galaxyId;


}
