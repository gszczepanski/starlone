package szczepanski.gerard.starlone.galaxy.application.galaxy

import szczepanski.gerard.starlone.commons.id.DomainId
import szczepanski.gerard.starlone.galaxy.application.galaxy.read.GalaxyReadRepository

class PlanetServiceSpec extends spock.lang.Specification implements PlanetObjectMother {

    GalaxyReadRepository galaxyReadRepository = Mock()
    GalaxyRepository galaxyWriteRepository = Mock()
    GalaxyCreationService galaxyCreationService = Mock()
    GalaxyEvents events = Mock()
    PlanetService planetService = new PlanetService(galaxyReadRepository, galaxyWriteRepository, galaxyCreationService, events)

    def "should create galaxy if there are no free planets in the galaxy for new user"() {
        given:
        DomainId newUserId = DomainId.create()
        1 * galaxyReadRepository.findFirstFreePlanet() >> Optional.empty()
        1 * galaxyReadRepository.findFirstFreePlanet() >> Optional.of(smallPlanet())

        when:
        planetService.assignPlanetForNewUser(newUserId)

        then:
        1 * galaxyCreationService.createDefaultGalaxy()
    }

    def "should assign user to first free planet found"() {
        given:
        DomainId newUserId = DomainId.create()
        Planet planet = smallPlanet()
        galaxyReadRepository.findFirstFreePlanet() >> Optional.of(planet)

        when:
        planetService.assignPlanetForNewUser(newUserId)

        then:
        planet.ownerUserId == newUserId
        1 * galaxyWriteRepository.updatePlanet(planet)
    }

    def "should send event with planet id after assign user to first free planet found"() {
        given:
        DomainId newUserId = DomainId.create()
        Planet planet = smallPlanet()
        galaxyReadRepository.findFirstFreePlanet() >> Optional.of(planet)

        when:
        planetService.assignPlanetForNewUser(newUserId)

        then:
        1 * events.planetAssignedToNewUser(planet.domainId)
    }

    def "should save updated planet with assigned user on assign new user to planet action"() {
        given:
        DomainId newUserId = DomainId.create()
        Planet planet = smallPlanet()
        galaxyReadRepository.findFirstFreePlanet() >> Optional.of(planet)

        when:
        planetService.assignPlanetForNewUser(newUserId)

        then:
        1 * galaxyWriteRepository.updatePlanet(planet)
    }
}
