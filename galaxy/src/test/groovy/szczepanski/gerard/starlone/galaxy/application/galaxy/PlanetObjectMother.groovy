package szczepanski.gerard.starlone.galaxy.application.galaxy

import static szczepanski.gerard.starlone.galaxy.application.galaxy.Planet.PlanetSize.*

trait PlanetObjectMother implements GalaxyObjectMother {

    def "smallPlanet"() {
        return new Planet(
                galaxy(),
                SMALL,
                new Coordinates(200, 400)
        )
    }

}